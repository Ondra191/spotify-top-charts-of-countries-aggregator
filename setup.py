import sys
import os

# credentials
CLIENT_ID = "49e615fa9ecb4f4db359863aa3508480"
CLIENT_SECRET = "d5682a431f1e4d71b8768077865a1be7"
REDIRECT_URI = "http://localhost:9000"

# url
SPOTIFY_CHARTS_URL = "https://spotifycharts.com/"
CATEGORY = "/regional"  # regional | viral
INTERVAL = "/weekly"  # weekly | daily
DATE = "/latest"  # latest | date in format '2021-05-23'

# number of top songs from each country charts
LIMIT_OF_TOP = 5

# id of playlist where to save songs
PLAYLIST_ID = "3IHr6shqncJwbdhGftghEN"

# authorization scopes needed for Spotify API
SCOPES = ["playlist-modify-public"]

# paths
PROJECT_FOLDER = os.path.dirname(sys.argv[0])
SPOTIFY_TOKEN_CACHE_PATH = PROJECT_FOLDER + "/resources/cache"
