# Spotify TOP charts of countries aggregator

Adds five most played songs from each regional chart in 
[spotifycharts](https://spotifycharts.com/regional) to specified Spotify playlist.

Gets latest, weekly most played songs from each country and 
ignores songs which are in TOP 200 Global playlist or are already included. 

## Setup

- **CLIENT_ID** - ID of your Spotify app
- **CLIENT_SECRET** - Secret of your Spotify app
- **REDIRECT_URI** - Redirect URI of your Spotify app
- **PLAYLIST_ID** - ID of playlist for charts songs
