import spotipy as spotipy
from spotipy import SpotifyOAuth, CacheFileHandler

from setup import PLAYLIST_ID, CLIENT_SECRET, REDIRECT_URI, CLIENT_ID, SCOPES, SPOTIFY_TOKEN_CACHE_PATH


def getSpotifyAPI():
    cacheHandler = CacheFileHandler(cache_path=SPOTIFY_TOKEN_CACHE_PATH)
    auth_manager = SpotifyOAuth(scope=SCOPES, client_secret=CLIENT_SECRET,
                                client_id=CLIENT_ID, redirect_uri=REDIRECT_URI,
                                cache_handler=cacheHandler)

    return spotipy.Spotify(auth_manager=auth_manager)


def getCurrentSongsInPlaylist(spotify):
    currentSongsIds = set()

    offset = 0
    limit = 100
    remainingData = True
    while remainingData:
        playlist = spotify.playlist_items(PLAYLIST_ID, offset=offset, limit=limit)

        tracks = playlist["items"]
        for track in tracks:
            currentSongsIds.add(track["track"]["id"])

        offset += limit

        if len(tracks) < limit:
            remainingData = False

    return currentSongsIds


def divideSongsIdsToChunks(idsOfSongs):
    """
    Because Spotify API has limit max 100 per request
    """
    chunks = []

    for i, songId in enumerate(idsOfSongs):
        indexInChunk = i % 100
        if indexInChunk == 0:
            chunks.append([])
        chunks[-1].append(songId)

    return chunks


def removeOldSongs(spotify, idsOfUpdatedSongs, idsOfCurrentSongs):
    toRemove = list(idsOfCurrentSongs.difference(idsOfUpdatedSongs))
    if 0 < len(toRemove) < 100:
        spotify.playlist_remove_all_occurrences_of_items(PLAYLIST_ID, toRemove)
    elif len(toRemove) >= 100:
        chunksOfSongs = divideSongsIdsToChunks(toRemove)
        for chunk in chunksOfSongs:
            spotify.playlist_remove_all_occurrences_of_items(PLAYLIST_ID, chunk)


def addNewSongs(spotify, idsOfUpdatedSongs, idsOfCurrentSongs):
    toAdd = list(idsOfUpdatedSongs.difference(idsOfCurrentSongs))
    if 0 < len(toAdd) < 100:
        spotify.playlist_add_items(PLAYLIST_ID, toAdd)
    elif len(toAdd) >= 100:
        chunksOfSongs = divideSongsIdsToChunks(toAdd)
        for chunk in chunksOfSongs:
            spotify.playlist_add_items(PLAYLIST_ID, chunk)


def updateSpotifyPlaylist(idsOfUpdatedSongs: set):
    print("Starting spotify API integration...")
    spotify = getSpotifyAPI()

    print("Getting all songs in specified playlist...")
    idsOfCurrentSongs: set = getCurrentSongsInPlaylist(spotify)

    print("Removing old songs...")
    removeOldSongs(spotify, idsOfUpdatedSongs, idsOfCurrentSongs)

    print("Adding new songs...")
    addNewSongs(spotify, idsOfUpdatedSongs, idsOfCurrentSongs)

    print("Spotify API integration finished")
