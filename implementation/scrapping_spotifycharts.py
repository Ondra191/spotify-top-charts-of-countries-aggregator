from selenium.webdriver.chrome.options import Options
from selenium.webdriver.remote.webelement import WebElement
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from setup import SPOTIFY_CHARTS_URL, LIMIT_OF_TOP, CATEGORY, INTERVAL, DATE


def getWebDriver():
    options = Options()
    options.add_argument("--headless")
    return webdriver.Chrome(ChromeDriverManager().install(), options=options)


def getTop200GlobalTracks(webDriver):
    webDriver.get(SPOTIFY_CHARTS_URL + CATEGORY + "/global" + INTERVAL + DATE)
    linksToTracks = webDriver.find_elements_by_xpath("//a[contains(@href, 'https://open.spotify.com/track/')]")

    top200GlobalTracks = []
    for el in linksToTracks:
        url: str = el.get_attribute("href")
        trackId = url.split("/")[-1]
        top200GlobalTracks.append(trackId)

    return top200GlobalTracks


def getTopTracks(webDriver, country, idsOfTopGlobalSongs, idsOfSongs):
    webDriver.get(SPOTIFY_CHARTS_URL + CATEGORY + country + INTERVAL + DATE)
    linksToTracks = webDriver.find_elements_by_xpath("//a[contains(@href, 'https://open.spotify.com/track/')]")

    topUniqueTracks = []

    for el in linksToTracks:
        if len(topUniqueTracks) == LIMIT_OF_TOP:
            break

        url: str = el.get_attribute("href")
        trackId = url.split("/")[-1]

        if trackId not in idsOfTopGlobalSongs and trackId not in idsOfSongs:
            topUniqueTracks.append(trackId)

    return topUniqueTracks


def getCountriesWithCharts(webDriver):
    webDriver.get(SPOTIFY_CHARTS_URL)
    specificLiElement: WebElement = webDriver.find_element_by_xpath("//li[@data-value='global']")
    parentUlElement = specificLiElement.find_element_by_xpath("./..")
    listOfLiElements = parentUlElement.find_elements_by_xpath(".//*")
    countries = [li.get_attribute("data-value") for li in listOfLiElements]
    countries.pop(0)  # remove global

    return countries


def getTopSongs():
    print("Starting scrapping of spotifycharts...")

    webDriver = getWebDriver()

    idsOfTopGlobalSongs = getTop200GlobalTracks(webDriver)
    idsOfSongs: set = set()

    print("Getting spotify countries with charts...")
    countries = getCountriesWithCharts(webDriver)

    print("Starting scrapping each country:")
    for i, country in enumerate(countries, 1):
        print(f"{i:02d}/{len(countries)}     {country}")
        urlCountry = "/" + country
        idsOfSongs.update(getTopTracks(webDriver, urlCountry, idsOfTopGlobalSongs, idsOfSongs))

    webDriver.quit()

    print("Scrapping of spotifycharts finished\n")

    return idsOfSongs
