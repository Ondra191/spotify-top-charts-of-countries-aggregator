from implementation.scrapping_spotifycharts import getTopSongs
from implementation.update_playlist import updateSpotifyPlaylist


def run():
    idsOfSongs = getTopSongs()
    updateSpotifyPlaylist(idsOfSongs)


if __name__ == "__main__":
    run()
